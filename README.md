# deezerDL

Just a little script in NodeJS to download everything from deezer

## Usage

- Get your compiled file [on the release page](https://github.com/Ghostfly/deezDL/releases)

Call it like that :

```
./deez-macos deezerUrl
```

Example : 

```
./deez-macos http://www.deezer.com/track/7435007
```

## Dev :
- Install NodeJS
- git clone the repo
- npm i

### Test :

```
npm test
```

### Compile :
- Install [Pkg](https://github.com/zeit/pkg)

```
npm install -g pkg
```

- Call it :

```
pkg deez.js --targets latest-macos-x64,latest-win-x86,latest-linux-x64 --out-dir compile
```

- Your compiled file is in the compile/ directory ;)
